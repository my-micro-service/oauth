package cn.smile.oauth;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * OAth权限
 * </p>
 *
 * @author 龙逸
 * @since 2020-08-20
 */
@SpringBootApplication
@MapperScan(basePackages = "cn.smile.repository.core.mapper")
public class AuthApplication {

    private static final Logger logger = LoggerFactory.getLogger(AuthApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);
        logger.info("---------------------AuthApplication Successful Start---------------------");
    }
}

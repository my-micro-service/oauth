package cn.smile.oauth.service.impl;

import cn.smile.commons.bean.domain.core.CoreAdmin;
import cn.smile.commons.bean.domain.core.CoreUser;
import cn.smile.repository.core.mapper.CoreAdminMapper;
import cn.smile.repository.core.mapper.CoreUserMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 自定义认证与授权
 * </>
 *
 * @author 龙逸
 * @since: 2020-10-28 13:46:35
 **/
public class UserDetailsServiceImpl implements UserDetailsService {

    @Resource
    private CoreAdminMapper coreAdminMapper;

    @Resource
    private CoreUserMapper coreUserMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 管理后台
        LambdaQueryWrapper<CoreAdmin> adminWrapper = new LambdaQueryWrapper<>();
        adminWrapper.eq(CoreAdmin::getUsername, username);
        CoreAdmin coreAdmin = coreAdminMapper.selectOne(adminWrapper);
        if (null != coreAdmin) {
            // 授权，管理员权限为 ADMIN
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            grantedAuthorities.add(new SimpleGrantedAuthority("ADMIN"));

            // 由框架完成认证工作
            return new User(coreAdmin.getUsername(), coreAdmin.getPassword(), grantedAuthorities);
        }

        // 门户网站
        LambdaQueryWrapper<CoreUser> userWrapper = new LambdaQueryWrapper<>();
        userWrapper.eq(CoreUser::getUsername, username);
        CoreUser coreUser = coreUserMapper.selectOne(userWrapper);
        if (null != coreUser) {
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            grantedAuthorities.add(new SimpleGrantedAuthority("USERS"));
            return new User(coreUser.getUsername(), coreUser.getPassword(), grantedAuthorities);
        }

        return null;
    }
}

package cn.smile.oauth.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.smile.commons.exceptions.BusinessException;
import cn.smile.commons.response.ResponseCode;
import cn.smile.commons.utils.RedisUtil;
import cn.smile.oauth.service.ILoginService;
import com.alibaba.fastjson.JSON;
import lombok.Data;
import org.redisson.api.RBucket;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 登录
 * </>
 *
 * @author 龙逸
 * @since: 2020-10-28 13:59:42
 **/
@Service
public class LoginServiceImpl implements ILoginService {
    /**
     * Token有效时间
     */
    private static final Long TOKEN_EFFECTIVE_TIME = 3600L;
    private static final Long TOKEN_DELETE_TIME = 0L;

    @Value("${security.oauth2.client.access-token-uri}")
    private String accessTokenUri;

    @Value("${security.oauth2.client.client-id}")
    private String clientId;

    @Value("${security.oauth2.client.client-secret}")
    private String clientSecret;

    @Resource
    private BCryptPasswordEncoder passwordEncoder;

    @Resource(name = "userDetailsServiceBean")
    private UserDetailsService userDetailsService;

    @Override
    public Map<String, String> getToken(String username, String password) {
        Map<String, String> result = new HashMap<>(1);

        // 验证密码是否正确
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        if (userDetails == null || !passwordEncoder.matches(password, userDetails.getPassword())) {
            throw new BusinessException(ResponseCode.USER_LOGIN_ERROR);
        }

        // 通过 HTTP 客户端请求登录接口
        Map<String, Object> authParam = getAuthParam();
        authParam.put("username", username);
        authParam.put("password", password);
        authParam.put("grant_type", "password");

        // 获取 access_token
        String strJson = HttpUtil.post(accessTokenUri, authParam);
        JSONObject jsonObject = JSONUtil.parseObj(strJson);
        String token = String.valueOf(jsonObject.get("access_token"));
        String refresh = String.valueOf(jsonObject.get("refresh_token"));
        if (StrUtil.isNotBlank(token) && StrUtil.isNotBlank(refresh)) {
            // 将 refresh_token 保存在服务端
            TokenEntity entity = new TokenEntity();
            entity.setUsername(username);
            entity.setRefreshToken(refresh);
            RedisUtil.setObject(token, JSON.toJSONString(entity), TimeUnit.SECONDS, TOKEN_EFFECTIVE_TIME);

            // 将 access_token 返回给客户端
            result.put("token", token);
            return result;
        }

        return null;
    }

    @Override
    public Map<String, String> refresh(String accessToken) {
        Map<String, String> result = new HashMap<>(1);

        // Access Token 不存在直接返回 null
        RBucket<String> bucket = RedisUtil.getBucket(accessToken);
        if (Objects.isNull(bucket) || StringUtils.isEmpty(bucket.get())) {
            throw new BusinessException(ResponseCode.USER_NOT_LOGGED_IN);
        }

        TokenEntity entity = JSON.parseObject(bucket.get(), TokenEntity.class);

        // 通过 HTTP 客户端请求登录接口
        Map<String, Object> authParam = getAuthParam();
        authParam.put("grant_type", "refresh_token");
        authParam.put("refresh_token", entity.getRefreshToken());

        // 获取 access_token
        String strJson = HttpUtil.post(accessTokenUri, authParam);
        JSONObject jsonObject = JSONUtil.parseObj(strJson);
        String token = String.valueOf(jsonObject.get("access_token"));
        String refresh = String.valueOf(jsonObject.get("refresh_token"));
        if (StrUtil.isNotBlank(token) && StrUtil.isNotBlank(refresh)) {

            RedisUtil.setObject(accessToken, null, TimeUnit.SECONDS, TOKEN_DELETE_TIME);
            entity.setRefreshToken(refresh);
            // 将 refresh_token 保存在服务端
            RedisUtil.setObject(token, JSON.toJSONString(entity), TimeUnit.SECONDS, TOKEN_EFFECTIVE_TIME);

            // 将 access_token 返回给客户端
            result.put("token", token);
            return result;
        }

        return null;
    }

    // 私有方法 ------------------------------------------- Begin

    private Map<String, Object> getAuthParam() {
        Map<String, Object> param = new HashMap<>(2);
        param.put("client_id", clientId);
        param.put("client_secret", clientSecret);
        return param;
    }

    @Data
    static class TokenEntity {
        private String username;
        private String refreshToken;
    }
}

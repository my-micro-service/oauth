package cn.smile.oauth.controller.param;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 登录参数
 * </>
 *
 * @author 龙逸
 * @since: 2020-10-28 14:07:31
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class LoginParam implements Serializable {
    private static final long serialVersionUID = 7324206383656599510L;

    /**
     * 账号
     */
    private String username;

    /**
     * 密码
     */
    private String password;
}

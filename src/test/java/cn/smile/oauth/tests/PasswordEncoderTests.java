package cn.smile.oauth.tests;

import cn.smile.oauth.AuthApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * <p>
 * </>
 *
 * @author 龙逸
 * @since: 2020-10-28 13:18:43
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AuthApplication.class)
public class PasswordEncoderTests {

    private static final Logger logger = LoggerFactory.getLogger(PasswordEncoderTests.class);

    @Resource
    private BCryptPasswordEncoder passwordEncoder;

    @Test
    public void testPasswordEncoder() {
        logger.info("dashboard = {}", passwordEncoder.encode("dashboard"));
        logger.info("123456 = {}", passwordEncoder.encode("123456"));
        logger.info("portal = {}", passwordEncoder.encode("portal"));
    }
}

FROM registry.cn-hangzhou.aliyuncs.com/hhit/java8:min
MAINTAINER LongYi <smile_jt@qq.com>
ENV APP_VERSION 1.0.0-SNAPSHOT
RUN mkdir /app
COPY oauth-$APP_VERSION.jar /app/app.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-Duser.timezone=GMT+08", "-jar", "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=19191","/app/app.jar", "--spring.profiles.active=prod"]
EXPOSE 9191 19191